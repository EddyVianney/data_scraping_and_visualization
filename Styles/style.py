
style_table_data_conditional = [{
    'if' : {'column_id' : 'intitule'},
    'textAlign' : 'Left'
}] + [{
    'if' : {'row_index' : 'odd'},
    'backgroundColor':'rgb(248,248,248)'
}]

style_table_header = {
    'backgroundColor' : 'rgb(230,230,230)',
    'fontWeight' : 'bold'
}

style_title_info = {'background':'blue','color':'white','textAlign':'center','padding':'10px 0 10px 0'}

style_layout_figure = {'border':'1px solid #eee','box-shadow':'0 2px 2px #ccc','display':'inline-block','width':'48%'}

style_list_dropdown = {'width':'25%',
    'border':'1px solid #eee',
    'padding':'30px 30px 30px 120px',
    'box-shadow':'0 2px 2px #ccc'
}
