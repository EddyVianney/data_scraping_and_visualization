import requests as rq
from multiprocessing import Pool
from bs4 import BeautifulSoup as bs
#from utilitaires import *
import pandas as pd
import time
import csv
import re
import os
import json
from pprint import pprint


# point d'entre du site
BASE = 'http://www.journaldunet.com'

ths = [ ['nombre','% de la population','moyenne par ville'],
['nombre','% de la population','moyenne par ville'],
['nombre de familles','pourcentage de famille','moyenne par ville'],
['nombre de familles','pourcentage de famille','moyenne par ville'],
['population','% de la population','donnees nationales'],
['population','% de la population','donnees nationales']]

tds = [['Hommes','Femmes'],['Moins de 15 ans','15 - 29 ans','30 - 44 ans','45 - 59 ans','60 - 74 ans','75 ans et plus'],
['Familles monoparentales','Couples sans enfant','Couples avec enfant(s)'],
['Familles sans enfant','Familles avec un enfant','Familles avec deux enfants','Familles avec trois enfants','Familles avec quatre enfants ou plus'],
['Population étrangère','Hommes étrangers','Femmes étrangères','Moins de 15 ans étrangers','15-24 ans étrangers','25-54 ans étrangers','55 ans et plus étrangers']]

for th,td in zip(ths,tds):
    cles.append([x + '(' + y + ')' for y in td for x in  th])

# equivalent de flatten numpy
cles = [x for y in cles for x in y ]

# recuperation des liens
liens_villes =  pd.read_csv('liensVilles.csv')
liens = liens_villes['lien']

def parse_lien(lien):
    #ville = lien.split('|')[0]
    #lien = lien.split('|')[1]
    req = rq.get(BASE + lien + '/' + 'demographie')
    time.sleep(1)
    if req.status_code == 200:
        #keys = list(['ville','lien'])
        #values = list([ville,lien])
        # recuperer le contenu HTML
        soup = bs(req.content,'html.parser')
        # recuperer toutes les tables de chaque page
        tables = soup.findAll('table',class_='odTable odTableAuto')

        for table in tables[1:]:
            trs = table.findAll('tr')
            tds = [tr.findAll('td') for tr in trs[1:]]
            for td in tds:
                for i in range(1,4):
                    valeurs.append(td[i].text)

        return dict(zip(cles,valeurs))

if __name__ == '__main__':
    with Pool(10) as p:
        dicts = p.map(parse_lien,liens)
        for dict in dicts:
            dataFrame = pd.concat([dataFrame, pd.DataFrame(dict,index=[0])],axis=0)
    # convertir le dataFrame en fichier csv en preservant l'encodage
    dataFrame.to_csv('demographie.csv',index=False, encoding='utf-8')
