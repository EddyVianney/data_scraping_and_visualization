import requests as rq
from bs4 import BeautifulSoup as bs
import pandas as pd
import csv

BASE = 'http://www.journaldunet.com/management/ville/index/villes?page='
NOMBRE_PAGE = 708 #708 en realite

# colonnes à selectionner
colonnes = ['ville','lien']

# initialisation du dictionnaire
data = {ville:[] for ville in colonnes}

# recuperation des liens
for numero_pagination in range(1,NOMBRE_PAGE):
    url = BASE + str(numero_pagination)
    soup = bs(rq.get(url).content,'html.parser')
    liens = soup.findAll('a')

    for lien in liens:
         if lien['href'].find('/ville-') != -1:
             data['ville'].append(lien.text)
             data['lien'].append(lien['href'])

dataFrame = pd.DataFrame(data)
dataFrame.to_csv('liensVilles.csv', index=False, encoding='utf-8')
dataFrame.head()
print(data)


# recuperer les informations generales de chaque ville
