import requests as rq
from multiprocessing import Pool
from bs4 import BeautifulSoup as bs
from utilitaires import *
from pprint import pprint
import pandas as pd
import time
import csv
import re
import os

# point d'entre du site
BASE = 'http://www.journaldunet.com'

# nombre de liens (pour les tests)
NOMBRE_PAGE = 30 #708 en realite

# verifier qu'une session de scrapping a deja ete lance
if os.path.isfile('villes_infos_generales.csv'):
    dataFrame    = pd.read_csv('villes_infos_generales.csv')
    liens_villes = pd.read_csv('liensVilles.csv',nrows=10)

    if len(dataFrame.index) < len(liens_villes.index):
        liens_villes = pd.concat([liens_villes,dataFrame[['ville','lien']]],ignore_index=True)
        liens_villes.drop_duplicates(keep=False,inplace=True)
    else:
        dataFrame = pd.DataFrame()
else:
    # aucune session de scrapping lancee
    liens_villes =  pd.read_csv('liensVilles.csv')
    # initialisation du dataFrame
    dataFrame = pd.DataFrame()

# stocker les villes et les liens dans la meme Serie
liens = liens_villes['ville'] +'|'+ liens_villes['lien']

def parse_lien(lien):
    ville = lien.split('|')[0]
    lien = lien.split('|')[1]
    req = rq.get(BASE + lien)
    time.sleep(1)
    if req.status_code == 200:
        keys = list(['ville','lien'])
        values = list([ville,lien])
        # recuperer le contenu HTML
        soup = bs(req.content,'html.parser')
        # recuperer toutes les tables de chaque page
        tables = soup.findAll('table',class_='odTable odTableAuto')

        for table in tables:
            trs = table.findAll('tr')
            tds = [tr.findAll('td') for tr in trs[1:]]
            for td in tds:
                results = nettoyer(td)
                key = results[0]
                value = results[1]
                keys.append('Nom des habitants' if 'Nom des habitants' in key else key)
                values.append(value)
        # on retourne les resultatas de chaque page
        return  dict(zip(keys,values))

if __name__ == '__main__':
    with Pool(10) as p:
        dicts = p.map(parse_lien,liens)
        for dict in dicts:
            dataFrame = pd.concat([dataFrame, pd.DataFrame(dict,index=[0])],axis=0)
    # convertir le dataFrame en fichier csv en preservant l'encodage
    dataFrame.to_csv('villes_infos_generales.csv',index=False, encoding='utf-8')
