import re
def diff(liste1, liste2):
    return set(liste1).symmetric_difference(set(liste2))

def nettoyer(td):
    x = re.search(r'\d+',td[0].text)
    if x is not None:
        # se placer deux caractères avant l'index du nombre (à cause de ()
        index = x.span()[0] - 2
        # recuperer la chaine privee du nombre
        key = td[0].text[0:index].strip()
        # recuperer le nombre trouve (l'annee)
        value = td[1].text + '-' + x.group()
    else:
        key = td[0].text
        value = td[1].text
    return [key,value]
