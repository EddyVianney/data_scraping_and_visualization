import dash_core_components as dcc
import dash_html_components as html
import dash_table
from Styles.style import *
infos_generales = dcc.Tab(label='Infos Generales',children =[
    html.Div([
        html.H3('Infos generales')
    ],style=style_title_info),
    html.Div([
        dash_table.DataTable(
            id ='table_infos',
            style_cell = {'font-family':'Montserrat'},
            style_data_conditional = style_table_data_conditional,
            style_header = style_table_header)

    ],style={'width':'40%','border':'1px solid #ccc','box-shadow':'0 2px 2px #ccc','display':'inline-block',
    'verticalAlign':'top','padding':'60px 30px 60px 30px'}),
    html.Div(id='map',style={'display':'inline-block','verticalAlign':'top','width':'50%','padding':'20px 0 20px 5px'})
])
