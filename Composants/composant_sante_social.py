import dash_core_components as dcc
import dash_html_components as html
import dash_table
from Styles.style import *


sante_social = dcc.Tab(label='Sante et Social',children =[

    html.Div([
        html.Div([
            html.H3('Sante',style=style_title_info)
        ]),
        html.Div([
                dcc.Graph(id='praticiens')
            ],style={'display':'inline-block','verticalAlign':'top','width':'60%'}),
        html.Div([
            dash_table.DataTable(id='tableau_praticiens',
                style_cell = {'font-family':'Montserrat'},
                style_data_conditional = style_table_data_conditional,
                style_header = style_table_header)
        ],style={'display':'inline-block','verticalAlign':'top','padding':'30px 0 0 15px','width':'30%'}),
        html.Div([
                dcc.Graph(id='etablissement')
            ],style={'display':'inline-block','verticalAlign':'top','width':'60%'}),
        html.Div([
                dash_table.DataTable(id='tableau_etablissements',
                    style_cell = {'font-family':'Montserrat'},
                    style_data_conditional = style_table_data_conditional,
                    style_header = style_table_header ),
        ],style={'display':'inline-block','verticalAlign':'top','padding':'30px 0 0 15px','width':'30%'})
    ]),
    html.Div([
        html.Div([
            html.H3('Social',style=style_title_info),
        ]),
        html.Div([
            dcc.Graph(id='caf')
        ],style=style_layout_figure),
        html.Div([
            dcc.Graph(id='rsa')
        ],style=style_layout_figure),
        html.Div([
            dcc.Graph(id='apl')
        ],style=style_layout_figure),
            html.Div([
                dcc.Graph(id='alloc')
            ],style=style_layout_figure)
    ])
])
