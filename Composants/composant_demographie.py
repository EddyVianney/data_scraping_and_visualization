import dash_core_components as dcc
import dash_html_components as html
import dash_table
from Styles.style import *

demographie = dcc.Tab(label='Demographie',children = [

    html.H3('Population française',style=style_title_info),
    html.Div([
        dcc.Graph(id='population')
    ],style={'border':'1px solid #eee','box-shadow':'0 2px 2px #ccc','display':'inline-block','verticalAlign':'top','width':'45%','padding':'50px 0 0 50px'}),
    html.Div([
        dcc.Graph(id='naissance_deces')
    ],style={'border':'1px solid #eee','box-shadow':'0 2px 2px #ccc','display':'inline-block','verticalAlign':'top','width':'45%','padding':'50px 0 0 50px'}),
    html.Div([
        dcc.Graph(id='repartion_par_sexe')
    ],style={'display':'inline-block','verticalAlign':'top','width':'35%'}),
    html.Div([
        dcc.Graph(id='reparition_par_age')
    ],style={'display':'inline-block','verticalAlign':'top','width':'35%'}),
    html.Div([
        dash_table.DataTable(
        id = 'repartitions',
        style_cell = {'font-family':'Montserrat'},
        style_data_conditional = [{
            'if' : {'column_id' : 'intitule'},
            'textAlign' : 'Left'
        }] + [{
            'if' : {'row_index' : 'odd'},
            'backgroundColor':'rgb(248,248,248)'
        }],
        style_header = {
            'backgroundColor' : 'rgb(230,230,230)',
            'fontWeight' : 'bold'
        }
        )
    ],style={'display':'inline-block','verticalAlign':'top','width':'25%','paddingTop':'50px'}),
    ##############
    html.Div([
        dcc.Graph(id='repartion_par_famille')
    ],style={'display':'inline-block','verticalAlign':'top','width':'35%'}),
    html.Div([
        dcc.Graph(id='reparition_par_statut_marital')
    ],style={'display':'inline-block','verticalAlign':'top','width':'35%'}),
    html.Div([
        dash_table.DataTable(
        id = 'repartitions2',
        style_cell = {'font-family':'Montserrat'},
        style_data_conditional = [{
            'if' : {'column_id' : 'intitule'},
            'textAlign' : 'Left'
        }] + [{
            'if' : {'row_index' : 'odd'},
            'backgroundColor':'rgb(248,248,248)'
        }],
        style_header = {
            'backgroundColor' : 'rgb(230,230,230)',
            'fontWeight' : 'bold'
        }
        )
    ],style={'display':'inline-block','verticalAlign':'top','width':'25%','paddingTop':'50px'}),

])
