import dash_core_components as dcc
import dash_html_components as html
import dash_table
from . composant_demographie import *
from . composant_sante_social import *
from . composant_infos_generales import *

liste_tables = html.Div([
    dcc.Tabs(id='tabs', value='tab-1',children = [
        infos_generales,
        demographie,
        sante_social,
        dcc.Tab(label='Entreprises'),
        dcc.Tab(label='Emploi'),
        dcc.Tab(label='Salaires'),
        dcc.Tab(label='CSP'),
        dcc.Tab(label='Automobiles'),
        dcc.Tab(label='Deliquance'),
        dcc.Tab(label='Europeennes')
    ])
])
