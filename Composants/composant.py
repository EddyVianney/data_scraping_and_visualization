import dash_core_components as dcc
import dash_html_components as html
import dash_table
import pandas as pd
import numpy as np
from . composant_demographie import *
from . composant_sante_social import *

#DATASETS    = 'Datasets'
#df_infos    = pd.read_csv('./Datasets' +'/' + 'infos.csv',dtype='unicode')
#df_liens    = pd.read_csv('./Datasets' +'/' + 'liensVilles.csv',dtype='unicode')
#villes = [{'label' : ville, 'value' : ville} for ville in df_liens['ville'].unique()]



infos_generales = dcc.Tab(label='Infos Generales',children =[
    html.Div([
        html.H3('Infos generales')
    ],style={'background':'blue','color':'white','textAlign':'center','padding':'10px 0 10px 0'}),
    html.Div([
        dash_table.DataTable(
            id ='table_infos',
            style_cell = {'font-family':'Montserrat'},
            style_data_conditional = [{
                'if' : {'column_id' : 'intitule'},
                'textAlign' : 'Left'
            }] + [{
                'if' : {'row_index' : 'odd'},
                'backgroundColor':'rgb(248,248,248)'
            }],
            style_header = {
                'backgroundColor' : 'rgb(230,230,230)',
                'fontWeight' : 'bold'
            }
        )
    ],style={'width':'40%','border':'1px solid #ccc','box-shadow':'0 2px 2px #ccc','display':'inline-block',
    'verticalAlign':'top','padding':'60px 30px 60px 30px'}),
    html.Div(id='map',style={'display':'inline-block','verticalAlign':'top','width':'50%','padding':'20px 0 20px 5px'})
])

liste_tables = html.Div([
    dcc.Tabs(id='tabs', value='tab-1',children = [
        infos_generales,
        demographie,
        sante_social,
        dcc.Tab(label='Entreprises'),
        dcc.Tab(label='Emploi'),
        dcc.Tab(label='Salaires'),
        dcc.Tab(label='CSP'),
        dcc.Tab(label='Automobiles'),
        dcc.Tab(label='Deliquance'),
        dcc.Tab(label='Europeennes')
    ])
])
