import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
from Styles.style import *

# recuperer les liens
df_liens    = pd.read_csv('./Datasets' +'/' + 'liensVilles.csv',dtype='unicode')
# extraire la liste des villes sans doublons
villes = [{'label' : ville, 'value' : ville} for ville in df_liens['ville'].unique()]

liste_deroulante = html.Div([
        html.H4('Selectionnez une ville'),
        dcc.Dropdown(
            id = 'ville-liste',
            options = villes,
            value = 'Paris (75000)')
], style=style_list_dropdown)
