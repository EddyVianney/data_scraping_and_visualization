# creer un environnement
conda create -n nom_env python=version

# activer l'environnement
conda activate 

# installer les librairies nécessaires
pip install dash plotly dash_table folium pandas numpy

# lancer le projet
python main.py
