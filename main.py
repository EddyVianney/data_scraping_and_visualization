import dash
import plotly
import plotly.graph_objs as go
from dash.dependencies import Input,Output
import os
import folium
from Composants.composant_liste_deroulante import *
from Composants.composant_liste_table import *
import pandas as pd
import numpy as np


DATASETS    = 'Datasets'
df_liens    = pd.read_csv(DATASETS +'/' + 'liensVilles.csv',dtype='unicode')
df_auto     = pd.read_csv(DATASETS +'/' + 'auto.csv',dtype='unicode')
df_chomage  = pd.read_csv(DATASETS +'/' + 'chomage.csv',dtype='unicode')
df_csp      = pd.read_csv(DATASETS +'/' + 'csp.csv',dtype='unicode')
df_delinqce = pd.read_csv(DATASETS +'/' + 'delinquance.csv',dtype='unicode')
df_demo     = pd.read_csv(DATASETS +'/' + 'demographie.csv',dtype='unicode')
df_elections = pd.read_csv(DATASETS +'/' + 'elections.csv',dtype='unicode')
df_emploi   = pd.read_csv(DATASETS +'/' + 'emploi.csv',dtype='unicode')
df_entrep   = pd.read_csv(DATASETS +'/' + 'entreprises.csv',dtype='unicode')
df_immo     = pd.read_csv(DATASETS +'/' + 'immobilier.csv',dtype='unicode')
df_infos    = pd.read_csv(DATASETS +'/' + 'infos.csv',dtype='unicode')
df_salaires = pd.read_csv(DATASETS +'/' + 'salaires.csv',dtype='unicode')
df_sante_social = pd.read_csv(DATASETS +'/' + 'santeSocial.csv',dtype='unicode')

dfs = {

    file.split('.')[0] : pd.read_csv('Datasets/' + file,dtype='unicode')
    for file in os.listdir('Datasets')
}


# application
app = dash.Dash(__name__)

#rajouter un layout
app.layout =  html.Div([
    liste_deroulante,
    liste_tables
])

@app.callback([Output('table_infos','data'), Output('table_infos','columns')],
                [Input('ville-liste','value')])
def update_generales(ville):
    df = dfs['infos']
    colonnes_non_retenues = ['Taux de chômage (2015)','Etablissement public de coopération intercommunale (EPCI)','lien',
                    'Unnamed: 0',"Pavillon bleu", "Ville d'art et d'histoire",
                    "Ville fleurie", "Ville internet",'ville']
    colonnes_retenues = [colonne for colonne in df.columns if colonne not in colonnes_non_retenues]

    data = pd.DataFrame(
    {
        'intitule': colonnes_retenues,
        'donnee' : [df[df['ville'] == ville][col].iloc[0] for col in colonnes_retenues]
    }).to_dict('rows')

    entete = {'id': 'intitule', 'name': "   "}, {'id': 'donnee', 'name': ville}

    return data, entete

@app.callback(Output('map','children'),[Input('ville-liste','value')])
def update_map(ville):
    df = dfs['infos']
    # recuperer les coordonnees
    latitude  = df[df['ville'] == ville]['Latitude'].iloc[0]
    longitude = df[df['ville'] == ville]['Longitude'].iloc[0]
    # creer la carte
    carte = folium.Map(location =(latitude,longitude))
    # creer un marqueur
    marqueur = folium.Marker(location = (latitude,longitude))
    # ajouter le marqueur sur la carte
    marqueur.add_to(carte)
    # nom du fichier stockant la carte
    fichier =  'Localisations/' + ville + '.html'
    # si le fichier n'existe pas encore, on sauvegarde la carte
    if not os.path.isfile(fichier):
        carte.save(fichier)
    return html.Iframe(srcDoc = open(fichier,'r').read(),width='100%',height='500px')

@app.callback(Output('population','figure'),[Input('ville-liste','value')])
def update_population_graph(ville):
    df = dfs['demographie']
    x_val = np.array(range(2006,2016))
    y_val = [
    valeur for cle, valeur in df.loc[df['ville'] == ville,'nbre habitants (2006)':'nbre habitants (2015)'].iloc[0].items()
    ]

    ville = ville.split()[0]
    traces = []
    traces.append(
            go.Scatter(
                x = x_val,
                y = y_val,
                mode = 'lines+markers',
                line = {'shape':'spline','smoothing':1}
            ))
    return {
        'data' : traces ,
        'layout' : go.Layout(
            title ='Evolution de la population de ' + ville,
            xaxis = {'title' : 'Annees'} ,
            yaxis = {'title' : 'Nombre d\'habitants'},
            hovermode ='closest',
            legend_orientation ='h' )}

@app.callback(Output('naissance_deces','figure'),[Input('ville-liste','value')])
def update_naissance_deces_graph(ville):
    df = dfs['demographie']
    x_val = np.array(range(1999,2017))
    y_val_naissances = [
        valeur for cle, valeur in df.loc[df['ville'] == ville,'nbre naissances (1999)':'nbre naissances (2016)'].iloc[0].items()
    ]
    y_val_deces = [
        valeur for cle, valeur in df.loc[df['ville'] == ville,'nbre deces (1999)':'nbre deces (2016)'].iloc[0].items()
    ]

    # On retire le code INSEE après avoir recupere les donnees
    ville = ville.split()[0]

    traces = [
            go.Scatter(
                x = x_val,
                y = y_val_naissances,
                mode = 'lines+markers',
                line = {'shape':'spline','smoothing':1},
                name ='naissances'
            ),
            go.Scatter(
                x = x_val,
                y = y_val_deces,
                mode = 'lines+markers',
                line = {'shape':'spline','smoothing':1},
                name = 'deces'
            )]
    return {
            'data' : traces,
            'layout' : go.Layout(
                title ='Evolution des naissances et des deces de ' + ville,
                xaxis = {'title' : 'Annees'} ,
                yaxis = {'title' : 'Nombre d\'individu'},
                hovermode ='closest',
                legend_orientation ='h' )}

@app.callback(Output('repartion_par_sexe','figure'),[Input('ville-liste','value')])
def update_repartion_par_sexe(ville):
    df = dfs['demographie']
    labels = ['Hommes','Femmes']
    values = [float(value) for key, value in  df.loc[df['ville'] == ville,'Hommes':'Femmes'].iloc[0].items()]
    traces = [
        go.Pie(labels = labels, values = values)
    ]
    total = sum(values)
    return  {
        'data' : traces,
        'layout' : go.Layout(
            title = 'Reparition par sexe<br>(Total : ' + str(total) + ')',
            legend_orientation ='h'
        )}

@app.callback(Output('reparition_par_age','figure'),[Input('ville-liste','value')])
def update_repartion_par_age(ville):
    df = dfs['demographie']
    labels = ['Moins de 15 ans','15 - 29 ans','30 - 44 ans','45 - 59 ans','60 - 74 ans','75 ans et plus']
    values = [float(value) for key, value in  df.loc[df['ville'] == ville,'Moins de 15 ans':'75 ans et plus'].iloc[0].items()]
    traces = [
        go.Pie(labels = labels, values = values)
    ]
    return  {
        'data' : traces,
        'layout' : go.Layout(
            title = 'Reparition par age<br>(Total : ' + str(sum(values)) + ')',
            legend_orientation ='h'
        )}

@app.callback([Output('repartitions','data'),Output('repartitions','columns')],[Input('ville-liste','value')])
def update_repartition(ville):
    df = dfs['demographie']
    colonnes = ['Hommes','Femmes','Moins de 15 ans','15 - 29 ans','30 - 44 ans','45 - 59 ans','60 - 74 ans','75 ans et plus']
    data = pd.DataFrame(
        {
            'intitule': colonnes,
            'donnee' : [df[df['ville'] == ville][col].iloc[0] for col in colonnes]
        }).to_dict('rows')

    entete = {'id': 'intitule', 'name': "   "}, {'id': 'donnee', 'name': ville.split()[0]}

    return data, entete

# repartition des familles
@app.callback(Output('repartion_par_famille','figure'),[Input('ville-liste','value')])
def update_repartition_par_familles(ville):
    df = dfs['demographie']
    labels =['Familles monoparentales','Couples sans enfant','Couples avec enfant','Familles sans enfant','Familles avec un enfant','Familles avec deux enfants','Familles avec trois enfants','Familles avec quatre enfants ou plus']
    values = [float(value) for key, value in  df.loc[df['ville'] == ville,labels].iloc[0].items()]
    traces = [
        go.Pie(labels = labels, values = values)
    ]
    total = sum(values)
    return  {
        'data' : traces,
        'layout' : go.Layout(
            title = 'Reparition par familles<br>(Total : ' + str(sum(values)) + ')',
            legend_orientation ='h'
        )}

@app.callback(Output('reparition_par_statut_marital','figure'),[Input('ville-liste','value')])
def update_repartition_par_statut_marital(ville):
    df = dfs['demographie']
    labels = ['Personnes célibataires','Personnes mariées','Personnes divorcées','Personnes veuves']
    values = [float(value) for key, value in  df.loc[df['ville'] == ville,labels].iloc[0].items()]
    traces = [
        go.Pie(labels = labels, values = values)
    ]
    total = sum(values)
    return  {
        'data' : traces,
        'layout' : go.Layout(
            title = 'Reparition par statut marital<br>(Total : ' + str(sum(values)) + ')',
            legend_orientation ='h'
        )}

@app.callback([Output('repartitions2','data'),Output('repartitions2','columns')],[Input('ville-liste','value')])
def update_repartition2(ville):
    df = dfs['demographie']
    colonnes =['Familles monoparentales','Couples sans enfant','Couples avec enfant','Familles sans enfant','Familles avec un enfant','Familles avec deux enfants','Familles avec trois enfants','Familles avec quatre enfants ou plus','Personnes célibataires','Personnes mariées','Personnes divorcées','Personnes veuves']
    data = pd.DataFrame(
        {
            'intitule': colonnes,
            'donnee' : [df[df['ville'] == ville][col].iloc[0] for col in colonnes]
        }).to_dict('rows')

    entete = {'id': 'intitule', 'name': "   "}, {'id': 'donnee', 'name': ville.split()[0]}

    return data, entete

# Sante et Social
@app.callback([Output('caf','figure'),Output('rsa','figure'),Output('apl','figure'),Output('alloc','figure')],[Input('ville-liste','value')])
def update_prestation(ville):
    df = dfs['santeSocial']
    x_val = np.array(range(2009,2018))
    colonnes = ['nbre allocataires ','nbre RSA ','nbre APL ','nbre Alloc Familiales ']
    titres = ['Evolution du nombre d\'allocataires CAF à ' + ville.split()[0],
    'Evolution du nombre de beneficiaires RSA à ' + ville.split()[0],
    'Evolution du nombre de beneficiaires de l\'aide au logement à ' + ville.split()[0],
    'Evolution du nombre de beneficiaires des allocations familiales à ' + ville.split()[0]]
    yaxis = [
        [float(value) for key, value in  df.loc[df['ville'] == ville, colonne + '(2009)':colonne + '(2017)'].iloc[0].items()]
        for colonne in colonnes]

    traces = [
         go.Scatter(
            x = x_val,
            y = y_val,
            mode = 'lines+markers',
            line = {'shape':'spline','smoothing':1})
             for y_val in yaxis]
    figs = [
           {'data' : [traces[i]],
            'layout' : go.Layout(
               title = titre,
                xaxis = {'title' : 'Annees'},
                yaxis = {'title' : 'Nombre d\'habitants'},
                hovermode ='closest',
                legend_orientation ='h')}
            for i,titre in enumerate(titres,start=0)]

    return figs

@app.callback(Output('praticiens','figure'),Input('ville-liste','value'))
def update_praticiens(ville):
    df = dfs['santeSocial']
    labels = ['Médecins généralistes','Masseurs-kinésithérapeutes','Dentistes','Infirmiers','Spécialistes ORL','Ophtalmologistes','Dermatologues','Sage-femmes','Pédiatres','Gynécologues','Pharmacies','Urgences']
    values = [float(value) for key, value in  df.loc[df['ville'] == ville,labels].iloc[0].items()]
    traces = [
        go.Pie(labels = labels, values = values)
    ]
    total = sum(values)
    return  {
        'data' : traces,
        'layout' : go.Layout(
            title = 'Praticiens<br>(Total : ' + str(sum(values)) + ')',
            legend_orientation ='h'
        )}

@app.callback([Output('tableau_praticiens','data'),Output('tableau_praticiens','columns')],[Input('ville-liste','value')])
def update_practiciens_tableau(ville):
    df = dfs['santeSocial']
    colonnes = ['Médecins généralistes','Masseurs-kinésithérapeutes','Dentistes','Infirmiers','Spécialistes ORL','Ophtalmologistes','Dermatologues','Sage-femmes','Pédiatres','Gynécologues','Pharmacies','Urgences']
    data = pd.DataFrame(
        {
            'intitule': colonnes,
            'donnee' : [df[df['ville'] == ville][col].iloc[0] for col in colonnes]
        }).to_dict('rows')

    entete = {'id': 'intitule', 'name': "   "}, {'id': 'donnee', 'name': ville.split()[0]}

    return data, entete

@app.callback(Output('etablissement','figure'),[Input('ville-liste','value')])
def update_etablissement(ville):
    df = dfs['santeSocial']
    labels = ['Urgences','Etablissements de santé de court séjour','Etablissements de santé de moyen séjour','Etablissements de santé de long séjour','Etablissement d\'accueil du jeune enfant','Maisons de retraite','Etablissements pour enfants handicapés','Etablissements pour adultes handicapés']
    values = [float(value) for key, value in  df.loc[df['ville'] == ville,labels].iloc[0].items()]
    traces = [
        go.Pie(labels = labels, values = values)
    ]
    total = sum(values)
    return  {
        'data' : traces,
        'layout' : go.Layout(
            title = 'Etablissements<br>(Total : ' + str(sum(values)) + ')',
            legend_orientation ='h'
        )}
@app.callback([Output('tableau_etablissements','data'),Output('tableau_etablissements','columns')],[Input('ville-liste','value')])
def update_tableau_etablissements(ville):
    df = dfs['santeSocial']
    colonnes = ['Urgences','Etablissements de santé de court séjour','Etablissements de santé de moyen séjour','Etablissements de santé de long séjour','Etablissement d\'accueil du jeune enfant','Maisons de retraite','Etablissements pour enfants handicapés','Etablissements pour adultes handicapés']
    data = pd.DataFrame(
        {
            'intitule': colonnes,
            'donnee' : [df[df['ville'] == ville][col].iloc[0] for col in colonnes]
        }).to_dict('rows')

    entete = {'id': 'intitule', 'name': "   "}, {'id': 'donnee', 'name': ville.split()[0]}

    return data, entete

server = app.server

#lancer le serveur
if __name__ == '__main__':
    app.run_server(debug=True)
